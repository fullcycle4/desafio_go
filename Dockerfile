FROM golang:latest as build

WORKDIR /app

COPY ./show-message.go .

RUN go build show-message.go

FROM scratch

COPY --from=build /app/show-message .

ENTRYPOINT ["./show-message"]